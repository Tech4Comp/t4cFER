# Basis-Image auf Python 3.9 aufbauen
FROM python:3.9-slim

# Arbeitsverzeichnis im Container erstellen und setzen
WORKDIR /app

# Kopiere die requirements.txt Datei in den Container
COPY requirements.txt .

# Installiere alle Abhängigkeiten
RUN pip install --no-cache-dir -r requirements.txt

# Kopiere den Rest des Projekts in den Container
COPY . .

# Standardbefehl, um das Projekt zu starten
CMD ["python", "keycloak_app.py"]
