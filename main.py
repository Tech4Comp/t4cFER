import queue
import cv2
import time
import threading
import tkinter as tk
from PIL import Image, ImageDraw, ImageTk
from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from insightface.app import FaceAnalysis
from hsemotion_onnx.facial_emotions import HSEmotionRecognizer
from influxdb_client.client.write_api import SYNCHRONOUS


class FERModel:
    def __init__(self, allow_gpu=True):
        provider = ['CUDAExecutionProvider', 'CPUExecutionProvider'] if allow_gpu else ['CPUExecutionProvider']
        self.__detection = FaceAnalysis(name='buffalo_sc', allowed_modules=['detection'],
                                        providers=provider)
        self.__detection.prepare(ctx_id=0, det_thresh=0.60, det_size=(640, 480))
        self.__classification = HSEmotionRecognizer(model_name='enet_b0_8_best_afew')

    def infer_emotion(self, frame):
        faces = self.__detection.get(frame)
        if faces:
            for face in faces:
                box = face.bbox.astype(int)
                x1, y1, x2, y2 = box[0:4]
                face_img = frame[y1:y2, x1:x2, :]
                emotion, scores = self.__classification.predict_emotions(face_img, logits=False)
                return emotion, scores, box
        return None, None, None


class FERView(tk.Tk):
    def __init__(self, firstname="Guest", uuid=""):
        tk.Tk.__init__(self)
        self.__presenter = FERPresenter(uuid)
        self.firstname = firstname
        self.uuid = uuid

        self.wm_title(f"Hallo {self.firstname}")
        self.container = tk.Frame(self)
        self.container.pack(side="top", fill="both", expand=True)
        self.panel = None
        self.record_button = tk.Button(self, text="Start Recording", command=self.record_pressed)
        self.record_button.pack(side="bottom", fill="both", pady=10, padx=10)
        self.protocol("WM_DELETE_WINDOW", self.close_application)

        self.__load_frame()

    def __load_frame(self):
        if not self.__presenter.img_queue.empty():
            image = ImageTk.PhotoImage(self.__presenter.img_queue.get())
            if self.panel is None:
                self.panel = tk.Label(self.container, image=image)
                self.panel.image = image
                self.panel.pack(side="top")
            else:
                self.panel.configure(image=image)
                self.panel.image = image
        self.after(5, self.__load_frame)

    def record_pressed(self):
        if self.__presenter.recording:
            self.record_button.config(text="Start Recording")
        else:
            self.record_button.config(text="Stop Recording")
        self.__presenter.switch_recording()

    def close_application(self):
        self.__presenter.release()
        self.destroy()


class FERPresenter:
    def __init__(self, uuid):
        self.img_queue = queue.Queue()
        self.recording = False
        self.__model = FERModel()
        self.__vc = cv2.VideoCapture(0)
        self.__worker_thread = threading.Thread(target=self.__update_frame)
        self.__worker_thread.start()

        # InfluxDB connection setup
        self.uuid = uuid
        self.influx_client = InfluxDBClient(
            url="http://localhost:8086",
            token="myfLt8Gf9pU8tsYY6R_dtJ2yOVjT3M_pQIOfGsjoGB9MnzLSF9oZ53HN59qyqwhmed1burqsC7zad_qBnyIjQg==",
            org="UniTest"
        )
        self.write_api = self.influx_client.write_api(write_options=SYNCHRONOUS)

    def __update_frame(self):
        while not self.recording:
            start_time = time.time()
            _, frame = self.__vc.read()
            frame_draw = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            draw = ImageDraw.Draw(frame_draw)
            emotion, _, box = self.__model.infer_emotion(frame)
            if box is not None:
                draw.rectangle(box.tolist(), outline=(255, 0, 0), width=6)
                draw.text(box.tolist(), emotion)
            fps = 1.0 / (time.time() - start_time)
            draw.text((0, 0), str(int(fps)))
            self.img_queue.put(frame_draw)
        return

    def __record_emotions(self):
        while self.recording:
            start_time = time.time()
            _, frame = self.__vc.read()
            _, emotion_values, _ = self.__model.infer_emotion(frame)
            if emotion_values is not None:
                self.write_emotion_to_influxdb(self.uuid, start_time, emotion_values)

            elapsed_time = time.time() - start_time
            time.sleep(max(float(0), 1.0 - elapsed_time))  # Write data every second
        return

    def write_emotion_to_influxdb(self, uuid, timestamp, emotion_values):
        try:

            timestamp_ns = int(timestamp * 1e9)

            point = Point("face_recognition") \
                .tag("uuid", uuid) \
                .field("emotion1", float(emotion_values[0])) \
                .field("emotion2", float(emotion_values[1])) \
                .field("emotion3", float(emotion_values[2])) \
                .field("emotion4", float(emotion_values[3])) \
                .field("emotion5", float(emotion_values[4])) \
                .field("emotion6", float(emotion_values[5])) \
                .field("emotion7", float(emotion_values[6])) \
                .field("emotion8", float(emotion_values[7])) \
                .time(timestamp_ns, WritePrecision.NS)  # Precision set to Nanoseconds

            self.write_api.write(bucket="face_recognition", org="UniTest", record=point)
            print(f"Data written to InfluxDB for timestamp: {timestamp}")
        except Exception as e:
            print(f"Error writing to InfluxDB: {e}")

    def switch_recording(self):
        self.recording = not self.recording
        self.__worker_thread.join()
        if self.recording:
            self.__worker_thread = threading.Thread(target=self.__record_emotions)
        else:
            self.__worker_thread = threading.Thread(target=self.__update_frame)
        self.__worker_thread.start()

    def release(self):
        self.recording = not self.recording
        self.__worker_thread.join()
        self.__vc.release()
        self.influx_client.close()


if __name__ == '__main__':
    # In the actual implementation, pass the firstname and uuid obtained from the auth system.
    app = FERView(firstname='Guest', uuid="your-uuid")
    app.mainloop()
