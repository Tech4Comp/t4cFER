import os
import subprocess
import webbrowser
import threading
import time
import tkinter as tk
from flask import Flask, request
from requests_oauthlib import OAuth2Session
import base64
import json
import psycopg2
import glob
import pandas as pd
from main import FERView
import hashlib

# Allow insecure transport for development purposes
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
os.environ['NO_ALBUMENTATIONS_UPDATE'] = '1'

# Load OIDC/OAuth2 Configuration from environment variables
options = {
    'url': 'https://auth.las2peer.org/auth/',
    'realm': 'main',
    'clientId': 'fer-client',
    'client_secret': os.environ.get('CLIENT_SECRET', 'default_secret'),  # Load from environment
    'redirect_uri': 'http://localhost:5000/callback',  # Use HTTPS in production
    'scope': 'email profile openid'
}

# Flask web server setup
app = Flask(__name__)

# Global variables for Tkinter root and token
root = None
token = None
user_uuid = None


# PostgreSQL database configuration
db_config = {
    'dbname': 'testdb',
    'user': 'postgres',
    'password': '123123',
    'host': 'localhost'
}

def hash_email(email):
    # Hash the email using SHA-256
    return hashlib.sha256(email.encode()).hexdigest()
def connect_db():
    return psycopg2.connect(**db_config)


def check_and_insert_user(email, firstname, lastname, uuid):
    try:
        with connect_db() as conn:
            with conn.cursor() as cur:

                # Hash the email before querying or inserting
                hashed_email = hash_email(email)

                # Specify the table name for 'id' to avoid ambiguity
                cur.execute("""
                    SELECT Person.id, Personmapping.uuid 
                    FROM Person 
                    INNER JOIN Personmapping 
                    ON Person.id = Personmapping.id 
                    WHERE email = %s
                """, (hashed_email,))

                result = cur.fetchone()
                if result:
                    return result[0], result[1]
                else:
                    # Insert the new user
                    cur.execute("""
                        INSERT INTO Person (email, firstname, lastname) 
                        VALUES (%s, %s, %s) RETURNING id
                    """, (hashed_email, firstname, lastname))

                    person_id = cur.fetchone()[0]

                    # Insert into Personmapping
                    cur.execute("""
                        INSERT INTO Personmapping (uuid, id) 
                        VALUES (%s, %s)
                    """, (uuid, person_id))

                    # Commit the transaction
                    conn.commit()
                    return person_id, uuid
    except Exception as e:
        print(f"Error in check_and_insert_user: {e}")
        return None, None


@app.route('/callback')
def callback():
    global oauth, token, user_uuid
    authorization_response = request.url
    print(f"Authorization response received: {authorization_response}")

    uuid = None

    try:
        token = oauth.fetch_token(token_url, authorization_response=authorization_response,
                                  client_secret=options['client_secret'])
        print("Access Token:", token['access_token'])

        # Decode and parse the token
        decoded_token = decode_jwt(token['access_token'])
        email = decoded_token.get('email')
        firstname = decoded_token.get('given_name')
        lastname = decoded_token.get('family_name')
        uuid = decoded_token.get('sub')

        if not uuid:
            raise ValueError("UUID not found in token")

        print(f"Starting face recognition for {firstname}")
        face_recognition = FERView(firstname=firstname, uuid=uuid)
        face_recognition.mainloop()

        # Check if user exists, otherwise insert
        person_id, user_uuid = check_and_insert_user(email, firstname, lastname, uuid)

        # Debugging Step
        if user_uuid:
            print(f"User UUID: {user_uuid}")

        root.quit()

        return """
        <html>
        <body>
            <p>Login successful! The tab will close automatically.</p>
            <script type="text/javascript">
                window.onload = function() {
                    setTimeout(function() {
                        window.open('', '_self').close();  // Close the current tab
                    }, 1000);  // Wait for 1 seconds before closing
                };
            </script>
        </body>
        </html>
        """


      #  return f'Login successful! Welcome {firstname}.'

    except Exception as e:
        return f"Error fetching token: {e}"




@app.route('/close_login')
def close_login():
    global root
    username = request.args.get('username')
    if root:
        root.quit()  # Close the login Tkinter window
        show_welcome_window(username)  # Show the welcome window
    return '', 204


def decode_jwt(token):
    parts = token.split('.')
    if len(parts) != 3:
        raise ValueError("Not a valid JWT token")

    payload = parts[1]
    padding = '=' * (4 - len(payload) % 4)
    payload += padding
    decoded = base64.urlsafe_b64decode(payload)
    return json.loads(decoded)


oauth = None
token_url = None

def login():
    global oauth, token_url
    auth_url = f"{options['url']}realms/{options['realm']}/protocol/openid-connect/auth"
    token_url = f"{options['url']}realms/{options['realm']}/protocol/openid-connect/token"

    oauth = OAuth2Session(client_id=options['clientId'], redirect_uri=options['redirect_uri'], scope=options['scope'])

    authorization_url, state = oauth.authorization_url(auth_url)
    print(f"Authorization URL: {authorization_url}")

    webbrowser.open(authorization_url)


def start_flask():
    app.run(port=5000, debug=True, use_reloader=False)

#def start_face_recognition(user_uuid):

  #  try:

       # print(f"Starting face recognition and passing UUID: {user_uuid}")

        #subprocess.run(["python", "main.py"], check=True)

        #subprocess.run(["python", "Sensordata.py", user_uuid], check=True)


   # except subprocess.CalledProcessError as e:
       # print(f"Error in starting face recognition: {e}")
 #   except FileNotFoundError:
        #print(f"main.py or Sensordata.py not found in the current directory.")

def start_login_process():
    global root
    root.after(1000, root.destroy)
    time.sleep(2)
    login_thread = threading.Thread(target=login)
    login_thread.start()

def get_firstname():
    return request.args.get('firstname')


def show_welcome_window(firstname):
    welcome_root = tk.Tk()
    welcome_root.title(f"Willkommen {firstname}!")
    welcome_label = tk.Label(welcome_root, text=f"Hallo {firstname}")
    welcome_label.pack(pady=20)

    welcome_root.after(5000, welcome_root.destroy)
    welcome_root.mainloop()

# Write a function to get the uuid from personmapping


if __name__ == "__main__":
    # Start Flask server in a separate thread
    flask_thread = threading.Thread(target=start_flask)
    flask_thread.start()

    # Create a simple Tkinter GUI
    root = tk.Tk()
    root.title("Login UI")

    login_button = tk.Button(root, text="Login", command=start_login_process)
    login_button.pack(pady=20)

    root.mainloop()